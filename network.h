volatile struct dev_net *dev_net;


struct page_pool{
  unsigned int* space;
  unsigned int base;
  unsigned int index;
  unsigned int capacity;
  unsigned int last;
};

// Initializes the network driver, allocating the space for the ring buffer.
void network_init();

// Starts receiving packets!
void network_start_receive();

// If opt != 0, enables interrupts when a new packet arrives.
// If opt == 0, disables interrupts.
void network_set_interrupts(int opt);

// Continually polls for data on the ring buffer. Loops forever!
void network_poll();

// Called when a network interrupt occurs.
void network_trap();


struct page_pool myPages;

// page pool mutex bit
extern volatile int mpage;
