#ifndef MUTEX_H_
#define MUTEX_H_

void mutex_lock(volatile int* m);

void mutex_unlock(volatile int* m);

#endif
