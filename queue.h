#ifndef QUEUE_H_
#define QUEUE_H_

#define QUEUE_RING_SIZE 2048
#define QUEUE_SIZE 1024

extern volatile int ms;
extern volatile int mv;
extern volatile int me;

struct queue {
	unsigned int base;
	unsigned int capacity;
	unsigned int head;
	unsigned int tail;
};

// General Task Queue entries
struct queue_entry{
  unsigned int valid;
  unsigned int identifer;
};
// Task queues
struct queue spammer_queue;
struct queue vulnerable_queue;
struct queue evil_queue;


void queue_init();

void queue_put(struct queue* queue, volatile int* m, unsigned int valid, unsigned int identifer);

void queue_get(struct queue* queue, struct queue_entry* qentry);

void queue_print(struct queue* queue);

#endif
