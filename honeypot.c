#include "longlong.h"
#include "honeypot.h"
#include "kernel.h"

 /* Honeypot statistics*/
unsigned int total_packets=0;
unsigned int total_bytes=0; 
unsigned int test1=0;
unsigned int test2=0;

void get1(){
    printf("Start: %u\n", test1);
}

void get2(){
    printf("End: %u\n", test2);
}

void set1(unsigned int v){
        test1=v;
}
void set2(unsigned int v){
        test2=v;
}


unsigned short switch_short(unsigned short num){
    return (num>>8) | (num<<8);
}
/*  Cite from stackoverflow.com/questions/7279393/quickest-way-to-change-endianness
*/
unsigned int switch_int(unsigned int num){
    return ((num>>24)&0xff) | // move byte 3 to byte 0
                ((num<<8)&0xff0000) | // move byte 1 to byte 2
                ((num>>8)&0xff00) | // move byte 2 to byte 1
                ((num<<24)&0xff000000); // byte 0 to byte 3
}

void packet_increment(){
	total_packets++;
}

void byte_increment(unsigned int n){
	total_bytes+=n;
}

unsigned int get_packet_count(){
	return total_packets;
}

unsigned int get_byte_count(){
	return total_bytes;
}

/* not right*/
unsigned long hash(unsigned char *pkt, int n)
{
        //set1(current_cpu_cycles());
        unsigned long hash = 5381;
        int i=0;
        unsigned char * ptr = pkt;
        while (i<n-64){
            hash = 1185921*hash+35937*ptr[0]+1089*ptr[1]+33*ptr[2]+ptr[3];
            hash = 1185921*hash+35937*ptr[4]+1089*ptr[5]+33*ptr[6]+ptr[7];
            hash = 1185921*hash+35937*ptr[8]+1089*ptr[9]+33*ptr[10]+ptr[11];
            hash = 1185921*hash+35937*ptr[12]+1089*ptr[13]+33*ptr[14]+ptr[15];
            hash = 1185921*hash+35937*ptr[16]+1089*ptr[17]+33*ptr[18]+ptr[19];
            hash = 1185921*hash+35937*ptr[20]+1089*ptr[21]+33*ptr[22]+ptr[23];
            hash = 1185921*hash+35937*ptr[24]+1089*ptr[25]+33*ptr[26]+ptr[27];
            hash = 1185921*hash+35937*ptr[28]+1089*ptr[29]+33*ptr[30]+ptr[31];
            hash = 1185921*hash+35937*ptr[32]+1089*ptr[33]+33*ptr[34]+ptr[35];
            hash = 1185921*hash+35937*ptr[36]+1089*ptr[37]+33*ptr[38]+ptr[39];
            hash = 1185921*hash+35937*ptr[40]+1089*ptr[41]+33*ptr[42]+ptr[43];
            hash = 1185921*hash+35937*ptr[44]+1089*ptr[45]+33*ptr[46]+ptr[47];
            hash = 1185921*hash+35937*ptr[48]+1089*ptr[49]+33*ptr[50]+ptr[51];
            hash = 1185921*hash+35937*ptr[52]+1089*ptr[53]+33*ptr[54]+ptr[55];
            hash = 1185921*hash+35937*ptr[56]+1089*ptr[57]+33*ptr[58]+ptr[59];
            hash = 1185921*hash+35937*ptr[60]+1089*ptr[61]+33*ptr[62]+ptr[63];
            
            ptr = (unsigned char*)(ptr+64);
            i+=64;
        }

        while (i<n){
        	hash=hash*33+pkt[i++];
        }
        return hash;

}

void print_stats(){
    printf_im("Total number of packets received: %u\n", total_packets);
    printf_im("Total number of bytes received: %u\n", total_bytes);
    printf_im("The reception rate is %u/%u mbps/s.\n", total_bytes*8, current_cpu_cycles());
}
