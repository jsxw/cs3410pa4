#include "queue.h"
#include "kernel.h"
#include "honeypot.h"
#include "mutex.h"
volatile int ms = 0;
volatile int mv = 0;
volatile int me = 0;


void queue_init(){
	struct queue_entry* qentrys = (struct queue_entry*) malloc(sizeof(struct queue_entry) * QUEUE_RING_SIZE);
   	spammer_queue.base=(unsigned int)virtual_to_physical(qentrys); 
    spammer_queue.capacity=QUEUE_RING_SIZE;
    spammer_queue.head=0;
    spammer_queue.tail=0;
    ms=0;

    struct queue_entry* qentryv = (struct queue_entry*) malloc(sizeof(struct queue_entry) * QUEUE_RING_SIZE);
   	vulnerable_queue.base=(unsigned int)virtual_to_physical(qentryv); 
    vulnerable_queue.capacity=QUEUE_RING_SIZE;
    vulnerable_queue.head=0;
    vulnerable_queue.tail=0;
    mv=0;

    struct queue_entry* qentrye = (struct queue_entry*) malloc(sizeof(struct queue_entry) * QUEUE_RING_SIZE);
   	evil_queue.base=(unsigned int)virtual_to_physical(qentrye); 
    evil_queue.capacity=QUEUE_RING_SIZE;
    evil_queue.head=0;
    evil_queue.tail=0;
    me=0;
}


void queue_put(struct queue* queue, volatile int* m, unsigned int valid, unsigned int identifer){
	mutex_lock(m);

	unsigned int capacity=queue->capacity;
    unsigned int addr=(unsigned int)physical_to_virtual(queue->base)+(queue->tail % queue->capacity)*sizeof(struct queue_entry);

	__asm__ __volatile__(
		// read head
		"Producer: lw $8, 8(%[a]); \n\t"
		// read tail
		"lw $9, 12(%[a]); \n\t"
		// tail - head -> $10
		"sub $10, $9, $8; \n\t"
		// set head-tail != 0
		"slt $14, $10, %[b]; \n\t"
		// branch to store if not both met
		"bnez $14, STORE;"
		"sll $0, $0, 0; \n\t"
		"j Producer; \n\t"
		"sll $0, $0, 0; \n\t"
		// increase tail by 1
		"STORE: sw %[c], 0(%[d]); \n\t"
		"sw %[e], 4(%[d]); \n\t"
		"addiu $9, $9, 1; \n\t"
		"sw $9, 12(%[a]); \n\t"
		:
		:[a]"r"(queue), [b]"r"(capacity), [c]"r"(valid), [d]"r"(addr), [e]"r"(identifer)
	);

	mutex_unlock(m);

}

void queue_get(struct queue* queue, struct queue_entry* qentry){

  	while(queue->head == queue->tail){
  	}

  	unsigned int head_idx = queue->head % queue->capacity;
  	struct queue_entry* temp = (struct queue_entry*) (physical_to_virtual (queue->base) + sizeof(struct queue_entry)* head_idx);
  	qentry->valid=temp->valid;
  	qentry->identifer=temp->identifer;
  	queue->head++;
  }

void queue_print(struct queue* queue){
	printf_im("The queue's head is: %u\n",queue->head);
	printf_im("The queue's tail is: %u\n",queue->tail);
	printf_im("The queue has capacity : %u\n",queue->capacity);
}


