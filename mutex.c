#include "kernel.h"
void mutex_lock(int* m) {
	//puts("try to acquire lock");
	unsigned int maddr=(unsigned int)m;
	__asm__ __volatile__(
		// test and set
		"test_and_set: li $8, 1; \n\t"
		// load from $a0
		".set mips2; \n\t"
		"ll $9, 0(%[l]); \n\t"
		// branch if lock is locked
		"bnez $9, test_and_set; \n\t"
		"sll $0, $0, 0; \n\t"
		// try to store 1
		"sc $8, 0(%[l]); \n\t"
		// branch if not successful
		"beqz $8, test_and_set; \n\t"
		"sll $0, $0, 0;"
		:
		: [l]"r"(maddr)
	);

}

void mutex_unlock(int* m) {
	unsigned int maddr=(unsigned int)m;
	__asm__ __volatile__(
		"sw $0, 0(%[u])"
		:
		:[u]"r"(maddr)
	);
}
