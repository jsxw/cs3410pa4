#include "kernel.h"
#include "honeypot.h"
#include "ringbuffer.h"
//#include "network.h"

// Network device driver.

// a pointer to the memory-mapped I/O region for the keyboard
//volatile struct dev_net *dev_net;

// Number of ring slots
#define RING_SIZE 16
// Ring slot size
#define BUFFER_SIZE 4096

volatile int mpage=0;
/*
struct pages{
  void** space;
  unsigned int index;
  unsigned int capacity;
  unsigned int last;
};

struct pages myPages;*/

void network_init()
{
  myPages.capacity = 300;
  myPages.space=(unsigned int*)malloc(sizeof(unsigned int)*myPages.capacity);
  myPages.base=(unsigned int)virtual_to_physical(myPages.space);

  for (int i=0; i<myPages.capacity; i++){
      myPages.space[i] = (unsigned int)virtual_to_physical(alloc_pages(1)); //alloc vs calloc
  }
  myPages.index = 0;
  myPages.last = myPages.capacity;

  /* Find out where I/O region is in memory. */
  for (int i = 0; i < 16; i++) {
    if (bootparams->devtable[i].type == DEV_TYPE_NETWORK) {
      puts("Detected network device...");
      // find a virtual address that maps to this I/O region
      dev_net = physical_to_virtual(bootparams->devtable[i].start);
      
      struct dma_ring_slot* ring = (struct dma_ring_slot*) malloc(sizeof(struct dma_ring_slot) * RING_SIZE);

      dev_net->rx_base=(unsigned int)virtual_to_physical(ring); //Physical?
      dev_net->rx_capacity=RING_SIZE;
      dev_net->rx_head=0;
      dev_net->rx_head=0;

      for (int i = 0; i < RING_SIZE; ++i) {
          //void* space = malloc(BUFFER_SIZE); 
          ring[i].dma_base = (unsigned int)virtual_to_physical(alloc_pages(1));
          ring[i].dma_len = BUFFER_SIZE; //?????????
      }
      dev_net->cmd=NET_SET_POWER;  //Power on first?
      dev_net->data=1;

      puts("...network driver is ready.");
      return;
    }
  }
}

void network_start_receive(){
    dev_net->cmd=NET_SET_RECEIVE;
    dev_net->data=1;
}

// If opt != 0, enables interrupts when a new packet arrives.
// If opt == 0, disables interrupts.
void network_set_interrupts(int opt){
    dev_net->cmd=NET_SET_INTERRUPTS;
    dev_net->data=opt;
}

// Continually polls for data on the ring buffer. Loops forever!
void network_poll(){
    while (1){
       /* if (dev_net->rx_tail>=dev_net->rx_head+dev_net->rx_capacity){
          printf("network ring is dangeours!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }*/
        if (dev_net->rx_head != dev_net->rx_tail) {
              //set1(current_cpu_cycles());
              int index=dev_net->rx_tail % dev_net->rx_capacity;
              struct dma_ring_slot* dma_packet=(struct dma_ring_slot*)((physical_to_virtual(dev_net->rx_base))+index*sizeof(struct dma_ring_slot));
              unsigned int tempbase=dma_packet->dma_base;
              unsigned int templen=dma_packet->dma_len;
              ringbuffer_put(tempbase, templen);
              //ringbuffer_print();
              //replace it an empty buffer

//            void* space = malloc(BUFFER_SIZE); 
//            dma_packet->dma_base = (unsigned int)virtual_to_physical(space);
              if (myPages.index==myPages.last){
                  dma_packet->dma_base=(unsigned int)virtual_to_physical(alloc_pages(1));
              }
              else {
              // printf("index=%u last=%u\n", myPages.index, myPages.last);
                  dma_packet->dma_base = myPages.space[myPages.index % myPages.capacity];
                  myPages.index++;
              }
              dma_packet->dma_len = BUFFER_SIZE;
              packet_increment();
              byte_increment(templen);
             // free(physical_to_virtual(tempbase));
              dev_net->rx_tail++;
              //set2(current_cpu_cycles());
        }
    }
}


