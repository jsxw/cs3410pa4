#include "kernel.h"
//#include "network.h"
#include "queue.h"
#include "hashtable.h"
#include "honeypot.h"
// Keyboard device driver.

// a pointer to the memory-mapped I/O region for the keyboard
volatile struct dev_kbd *dev_kbd;

void keyboard_init()
{
  /* Find out where I/O region is in memory. */
  for (int i = 0; i < 16; i++) {
    if (bootparams->devtable[i].type == DEV_TYPE_KEYBOARD) {
      puts("Detected keyboard device...");
      // find a virtual address that maps to this I/O region
      dev_kbd = physical_to_virtual(bootparams->devtable[i].start);
      // also allow keyboard interrupts
      set_cpu_status(current_cpu_status() | (1 << (8+INTR_KEYBOARD)));
      puts("...keyboard driver is ready.");
      return;
    }
  }
}

void keyboard_trap() {
  // note: interrupts should be off already
  // so long as there is a character to be read
  while (dev_kbd->status) {
    // read the character
    char c = dev_kbd->data;

    // then just print it
    putchar(c);

    if (c=='p'){
       print_stats();
    }
    else if (c=='r'){
      ringbuffer_print();
    }
    else if (c=='t'){
      printf_im("It has passed %d s.\n", (current_cpu_cycles()/CPU_CYCLES_PER_SECOND));
    }
    else if (c=='n'){
      printf_im("Network device buffer: head=%u tail=%u\n", dev_net->rx_head, dev_net->rx_tail);
    }
    else if (c=='q'){
      printf_im("This is spammer queue list: \n");
      queue_print(&spammer_queue);
      printf_im("This is evil queue list: \n");
      queue_print(&evil_queue);
      printf_im("This is vulnerable queue list: \n");
      queue_print(&vulnerable_queue);
    }
    else if (c=='h'){
      printf_im("Spammer hashtable stats:\n");
      hashtable_stats(&hashtable_spammer);
      printf_im("Evil hashtable stats:\n");
      hashtable_stats(&hashtable_evil);
      printf_im("vulnerable hashtable stats:\n");
      hashtable_stats(&hashtable_vulnerable);
    }
    else if (c=='s'){
      queue_put(&evil_queue, &me, 3, 0);
    }
    /*
    else if (c=='m'){
      get1();
      get2();
    }*/
  }
}


