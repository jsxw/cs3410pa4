#ifndef RINGBUFFER_H_
#define RINGBUFFER_H_


#define BUFFER_POOL_RING_SIZE 4096
#define BUFFER_POOL_SIZE 4096

extern volatile struct buffer buffer_pool;
extern volatile int mb;

//	Buffer pool
struct buffer {
	unsigned int rx_base;
	unsigned int rx_capacity;
	unsigned int rx_head;
	unsigned int rx_tail;
};

void ringbuffer_init();

void ringbuffer_put(unsigned int ptr, unsigned int len);

void ringbuffer_get(struct dma_ring_slot* slot);

void ringbuffer_print();


#endif
