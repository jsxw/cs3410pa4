#include "kernel.h"
#include "hashtable.h"
#include "mutex.h"

static int inPrinting_on_Spammer=0;
/**
 * Add a new element to arraylist
 */
void arraylist_add(arraylist* a, void* x){
    unsigned int len;
    len=a->length;
    unsigned int capacity;
    capacity=a->buffer_size;
    //if (len==capacity){
       // a->buffer=(void **) realloc(a->buffer, 2*capacity*sizeof(void*));
       // a->buffer_size=2*capacity;
    //}
    a->buffer[len]=x;
    a->length=a->length+1;
}
/**
 * Free memory of arraylist
 */
void arraylist_free(arraylist* a)
{
    free(a->buffer);
    free(a);
}
/**
 * Create a new arraylist
 */
arraylist* arraylist_new(){
    arraylist* a=(arraylist*)malloc(sizeof(arraylist));
    a->buffer=(void**)malloc(200*sizeof(void*));
    a->buffer_size=200;
    a->length=0;
    return a;
}
/**
 * Remove a element from arraylist
 */
void arraylist_remove(arraylist* a, unsigned int index){
    unsigned int i;
    for (i=index; i<a->length-1; i++){
        a->buffer[i]=a->buffer[i+1];
    }
    --a->length;
}
/**
 * Get element of given index from arraylist
 */
void* arraylist_get(arraylist* a, unsigned int index){
    return a->buffer[index];
}
/**
* Represent a pair of key and value
*/
typedef struct{
   unsigned int key;
   unsigned int value;
} pair;
/**
* Create a new pair of key and value
*/
pair* pair_new(unsigned int key, unsigned int value){
	pair* p=(pair*)malloc(sizeof(pair));
	p->key=key;
	p->value=value;
	return p;
}

/**
* Create a new hashtable
*/
void hashtable_create(){
    hashtable_spammer.load_factor=5;
    hashtable_spammer.num_of_elements=0;
    hashtable_spammer.table_size=100;
    hashtable_spammer.total_counts=0;
    hashtable_spammer.buckets=(arraylist**)malloc(hashtable_spammer.table_size*sizeof(arraylist*));
    unsigned int i;
    for (i=0; i<hashtable_spammer.table_size; i++){
        hashtable_spammer.buckets[i]=arraylist_new();
    }  

    hashtable_vulnerable.load_factor=5;
    hashtable_vulnerable.num_of_elements=0;
    hashtable_vulnerable.table_size=100;
    hashtable_vulnerable.total_counts=0;
    hashtable_vulnerable.buckets=(arraylist**)malloc(hashtable_vulnerable.table_size*sizeof(arraylist*));
    for (i=0; i<hashtable_vulnerable.table_size; i++){
        hashtable_vulnerable.buckets[i]=arraylist_new();
    }

    hashtable_evil.load_factor=5;
    hashtable_evil.num_of_elements=0;
    hashtable_evil.table_size=100;
    hashtable_evil.total_counts=0;
    hashtable_evil.buckets=(arraylist**)malloc(hashtable_evil.table_size*sizeof(arraylist*));
    for (i=0; i<hashtable_evil.table_size; i++){
        hashtable_evil.buckets[i]=arraylist_new();
    } 
}
/**
* Return hash value
*/
unsigned int hashtable_hash(unsigned int a){
    a=(a^61)^(a>>16);
    a=a+(a<<3);
    a=a^(a>>4);
    a=a * 0x27d4eb2d;
    a=a^(a>>15);
    return a;
}

void hashtable_update(struct hashtable *self, unsigned int key, int ind){
	if (ind==1){
		mutex_lock(&inPrinting_on_Spammer);
	}
	unsigned int index;
    index=hashtable_hash(key);
	index%=self->table_size;
	arraylist* a;
	a=self->buckets[index];
	unsigned int len=a->length;
	int i;
	for (i=0; i<len; i++){
	    pair* p=arraylist_get(a, i);
	    if (p->key==key){
	        p->value++;
	       	self->total_counts++;
	       	if (ind==1){
				mutex_unlock(&inPrinting_on_Spammer);
			}
	        return;
	    }
	}
	if (ind==1){
		mutex_unlock(&inPrinting_on_Spammer);
	}
}
/**
* Put a pair of key and value into hashtable; remove any duplicate key beforehand; may cause resizing
*/
void hashtable_put(struct hashtable *self, unsigned int key, unsigned int value, int ind){
	if (ind==1){
		mutex_lock(&inPrinting_on_Spammer);
	}
    unsigned int index;
    index=hashtable_hash(key);
	index%=self->table_size;
	arraylist* a;
	a=self->buckets[index];
	unsigned int len=a->length;
	int i;
	for (i=0; i<len; i++){
	    pair* p=arraylist_get(a, i);
	    if (p->key==key){
	    	if (ind==1){
				mutex_unlock(&inPrinting_on_Spammer);
			}
		    return;
	    }
	}
	pair* p;
	p=pair_new(key, value);
    arraylist_add(self->buckets[index], p);
	self->num_of_elements++;
	if (ind==1){
		mutex_unlock(&inPrinting_on_Spammer);
	}
}
/**
* Get value of corresponding key; exit with exit code 1 if unsuccessful
*/
int hashtable_get(struct hashtable * self, unsigned int key){
    unsigned int index;
    index=hashtable_hash(key);
	index%=self->table_size;
	arraylist* a;
	a=self->buckets[index];
	unsigned int len=a->length;
	int i;
	for (i=0; i<len; i++){
	    pair* p=arraylist_get(a, i);
	    if (p->key==key){
	        return (p->value);
	    }
	}
	return -1;
}
/**
* Remove value of corresponding key; exit with exit code 1 if unsuccessful
*/
void hashtable_remove(struct hashtable *self, unsigned int key, int ind){
	if (ind==1){
		mutex_lock(&inPrinting_on_Spammer);
	}
    unsigned int index;
    index=hashtable_hash(key);
	index%=self->table_size;
	arraylist* a;
	a=self->buckets[index];
	unsigned int len=a->length;
	int i;
	for (i=0; i<len; i++){
	    pair* p=arraylist_get(a, i);
	    if (p->key==key){
	        arraylist_remove(a, i);
	        self->num_of_elements--;
	        self->total_counts-=p->value;
	        free(p);
	        if (ind==1){
				mutex_unlock(&inPrinting_on_Spammer);
			}
            return;
	    }
	}
	if (ind==1){
		mutex_unlock(&inPrinting_on_Spammer);
	}
}
/**
* Print statistics of hashtable: number of elements, number of buckets and total number of insertions
*/
void hashtable_stats(struct hashtable *self){
    printf_im("length=%u, N=%u, counts=%u\n",self->num_of_elements, self->table_size, self->total_counts);
}

void hashtable_free(struct hashtable *self){
	int i;
	for (i=0; i<self->table_size; i++){
	    int j;
		arraylist* a=self->buckets[i];
		for (j=0; j<a->length; j++){
			free(arraylist_get(a, j));
		}
		arraylist_free(a);
	}
	free(self->buckets);
}
void hashtable_stats_table(){
	mutex_lock(&inPrinting_on_Spammer);
	puts("     count spam_source          count evil_hash          count vuln_port    ");
	puts("-----------------------------------------------------------------------------");
	int js=0;
	int ks=0;
	int je=0;
	int ke=0;
	int jv=0;
	int kv=0;
	int maxLen=hashtable_spammer.num_of_elements;
	if (maxLen<hashtable_evil.num_of_elements) maxLen=hashtable_evil.num_of_elements;
	if (maxLen<hashtable_vulnerable.num_of_elements) maxLen=hashtable_vulnerable.num_of_elements;
	for (int i=0; i<maxLen; i++){
		if (i<hashtable_spammer.num_of_elements){
		    while (ks>=hashtable_spammer.buckets[js]->length){
				ks=0;
				js++;
			}
			pair* ps=(pair*)arraylist_get(hashtable_spammer.buckets[js], ks);
			printf_im("   %8u  0x%08x  |", ps->value, ps->key);
			ks++;
		}
		else printf_im("                         |");
		if (i<hashtable_evil.num_of_elements){
		    while (ke>=hashtable_evil.buckets[je]->length){
				ke=0;
				je++;
			}
			pair* pe=arraylist_get(hashtable_evil.buckets[je], ke);
			printf_im("   %8u  0x%08x  |", pe->value, pe->key);
			ke++;
		}
		else printf_im("                         |");
		if (i<hashtable_vulnerable.num_of_elements){
		    while (kv>=hashtable_vulnerable.buckets[jv]->length){
				kv=0; 
				jv++;
			}
			pair* pv=(pair*)arraylist_get(hashtable_vulnerable.buckets[jv], kv);
			printf_im("     %8u  0x%04x  |", pv->value, pv->key);
			kv++;
		}
		else printf_im("                       |");
		printf_im("\n");
	}
	printf_im("   Total counts: %6u  |   Total counts: %6u  |  Total counts: %6u |\n", hashtable_spammer.total_counts, hashtable_evil.total_counts, hashtable_vulnerable.total_counts);
	printf_im("   Total entries: %5u  |   Total entries: %5u  |  Total entries: %5u |\n", hashtable_spammer.num_of_elements, hashtable_evil.num_of_elements, hashtable_vulnerable.num_of_elements);
	mutex_unlock(&inPrinting_on_Spammer);
}

