#include "kernel.h"
#include "ringbuffer.h"
#include "honeypot.h"
#include "queue.h"
#include "hashtable.h"
#include "mutex.h"

struct bootparams *bootparams;

int debug = 1; // change to 0 to stop seeing so many messages

void shutdown() {
  puts("Shutting down...");
  // this is really the "wait" instruction, which gcc doesn't seem to know about
  __asm__ __volatile__ ( ".word 0x42000020\n\t");
  while (1);
}


/* Trap handling.
 *
 * Only core 0 will get interrupts, but any core can get an exception (or
 * syscall).  So we allocate enough space for every core i to have its own place
 * to store register state (trap_save_state[i]), its own stack to use in the
 * interrupt handler (trap_stack_top[i]), and a valid $gp to use during
 * interrupt handling (trap_gp[i]).
 */

struct mips_core_data *trap_save_state[MAX_CORES]; /* one save-state per core */
void *trap_stack_top[MAX_CORES]; /* one trap stack per core */
unsigned int trap_gp[MAX_CORES]; /* one trap $gp per core */

void trap_init()
{
  int id = current_cpu_id();

  /* trap should use the same $gp as the current code */
  trap_gp[id] = current_cpu_gp();

  /* trap should use a fresh stack */
  void *bottom = alloc_pages(4);
  trap_stack_top[id] = bottom + 4*PAGE_SIZE - 4;

  /* put the trap save-state at the top of the corresponding trap stack */
  trap_stack_top[id] -= sizeof(struct mips_core_data);
  trap_save_state[id] = trap_stack_top[id];

  /* it is now safe to take interrupts on this core */
  intr_restore(1);
}

void interrupt_handler(int cause)
{
  // note: interrupts will only happen on core 0
  // diagnose the source(s) of the interrupt trap
  int pending_interrupts = (cause >> 8) & 0xff;
  int unhandled_interrupts = pending_interrupts;

  if (pending_interrupts & (1 << INTR_KEYBOARD)) {
    //if (debug) printf("interrupt_handler: got a keyboard interrupt, handling it\n");
    keyboard_trap();
    unhandled_interrupts &= ~(1 << INTR_KEYBOARD);
  }

  if (pending_interrupts & (1 << INTR_TIMER)) {
    printf("interrupt_handler: got a spurious timer interrupt, ignoring it and hoping it doesn't happen again\n");
    unhandled_interrupts &= ~(1 << INTR_TIMER);
  }

  if (unhandled_interrupts != 0) {
    printf("got interrupt_handler: one or more other interrupts (0x%08x)...\n", unhandled_interrupts);
  }


}

void trap_handler(struct mips_core_data *state, unsigned int status, unsigned int cause)
{
  if (debug) printf("trap_handler: status=0x%08x cause=0x%08x on core %d\n", status, cause, current_cpu_id());
  // diagnose the cause of the trap
  int ecode = (cause & 0x7c) >> 2;
  switch (ecode) {
    case ECODE_INT:	  /* external interrupt */
      interrupt_handler(cause);
      return; /* this is the only exception we currently handle; all others cause a shutdown() */
    case ECODE_MOD:	  /* attempt to write to a non-writable page */
      printf("trap_handler: some code is trying to write to a non-writable page!\n");
      break;
    case ECODE_TLBL:	  /* page fault during load or instruction fetch */
    case ECODE_TLBS:	  /* page fault during store */
      printf("trap_handler: some code is trying to access a bad virtual address!\n");
      break;
    case ECODE_ADDRL:	  /* unaligned address during load or instruction fetch */
    case ECODE_ADDRS:	  /* unaligned address during store */
      printf("trap_handler: some code is trying to access a mis-aligned address!\n");
      break;
    case ECODE_IBUS:	  /* instruction fetch bus error */
      printf("trap_handler: some code is trying to execute non-RAM physical addresses!\n");
      break;
    case ECODE_DBUS:	  /* data load/store bus error */
      printf("trap_handler: some code is read or write physical address that can't be!\n");
      break;
    case ECODE_SYSCALL:	  /* system call */
      printf("trap_handler: who is doing a syscall? not in this project...\n");
      break;
    case ECODE_BKPT:	  /* breakpoint */
      printf("trap_handler: reached breakpoint, or maybe did a divide by zero!\n");
      break;
    case ECODE_RI:	  /* reserved opcode */
      printf("trap_handler: trying to execute something that isn't a valid instruction!\n");
      break;
    case ECODE_OVF:	  /* arithmetic overflow */
      printf("trap_handler: some code had an arithmetic overflow!\n");
      break;
    case ECODE_NOEX:	  /* attempt to execute to a non-executable page */
      printf("trap_handler: some code attempted to execute a non-executable virtual address!\n");
      break;
    default:
      printf("trap_handler: unknown error code 0x%x\n", ecode);
      break;
  }
  shutdown();
}


/* kernel entry point called at the end of the boot sequence */
void __boot() {

  if (current_cpu_id() == 0) {
    /* core 0 boots first, and does all of the initialization */

    // boot parameters are on physical page 0
    bootparams = physical_to_virtual(0x00000000);

    // initialize console early, so output works
    console_init();

    // output should now work
    printf("Welcome to my kernel!\n");
    printf("Running on a %d-way multi-core machine\n", current_cpu_exists());

    // initialize memory allocators
    mem_init();

    // prepare to handle interrupts, exceptions, etc.
    trap_init();
    
    // initiailize network card
    network_init();
    network_set_interrupts(0); 

    // initialize large buffer pool
    ringbuffer_init();
    // initialize task queues
    queue_init();
    // initialize hashtable
    hashtable_create();

    // initialize keyboard late, since it isn't really used by anything else
    keyboard_init();

    // see which cores are already on
    for (int i = 0; i < 32; i++)
      printf("CPU[%d] is %s\n", i, (current_cpu_enable() & (1<<i)) ? "on" : "off");

    // turn on all other cores
    set_cpu_enable(0xFFFFFFFF);

    // see which cores got turned on
    busy_wait(0.1);
    for (int i = 0; i < 32; i++){
      printf("CPU[%d] is %s\n", i, (current_cpu_enable() & (1<<i)) ? "on" : "off");
    }
  } 

  if (current_cpu_id()==1){
    while (!current_cpu_enable()){
    }
    // Start polling
    network_start_receive();
    network_poll();
  }
  else if (current_cpu_id()==2){
      while (!current_cpu_enable()){
      }
      // Examine Spammer tasks
      while (1){ 
         if (spammer_queue.head!=spammer_queue.tail){
            struct queue_entry qentry;
            queue_get(&spammer_queue, &qentry);
            if (qentry.valid==0){
            hashtable_update(&hashtable_spammer, qentry.identifer, 1);
            }
            else if (qentry.valid==1){
              hashtable_put(&hashtable_spammer, qentry.identifer, 0, 0);
            }
            else if (qentry.valid==2){
              hashtable_remove(&hashtable_spammer, qentry.identifer,1);
            }
        }
    }
  }
  else if (current_cpu_id()==3){
    // Examine Evil tasks and Vulnerable tasks in turn
    while (1){
      if (evil_queue.head!=evil_queue.tail){
          struct queue_entry qentry;
          queue_get(&evil_queue, &qentry);
          if (qentry.valid==0){
            hashtable_update(&hashtable_evil, qentry.identifer, 0);
          }
          else if (qentry.valid==1){
            hashtable_put(&hashtable_evil, qentry.identifer, 0, 0);
          }
          else if(qentry.valid==2){
             hashtable_remove(&hashtable_evil, qentry.identifer,0);
          }
          else {
             hashtable_stats_table();
             print_stats();
          }
        }
        if (vulnerable_queue.head!=vulnerable_queue.tail){
          struct queue_entry qentry;
          queue_get(&vulnerable_queue, &qentry);
          if (qentry.valid==0){
            hashtable_update(&hashtable_vulnerable, qentry.identifer, 0);
          }
          else if (qentry.valid==1){
            hashtable_put(&hashtable_vulnerable, qentry.identifer, 0, 0);
          }
          else if(qentry.valid==2){
            hashtable_remove(&hashtable_vulnerable, qentry.identifer,0);
          }
        }
      }
    }
  else{
    // Take packet
    while (!current_cpu_enable()){
    }
    while (1){
       struct dma_ring_slot pkt;
       ringbuffer_get(&pkt);
       void* ptr=physical_to_virtual(pkt.dma_base);

       struct honeypot_command_packet* pkth=(struct honeypot_command_packet*)ptr;

       unsigned int srcaddr=switch_int(pkth->headers.ip_source_address_big_endian);
       unsigned short destport=switch_short(pkth->headers.udp_dest_port_big_endian);
       unsigned short secret=switch_short(pkth->secret_big_endian);
       // insert the three entries to corresponding list
       if (secret==HONEYPOT_SECRET){
          unsigned short cmd_little_endian=switch_short(pkth->cmd_big_endian);

          unsigned int data_little_endian=switch_int(pkth->data_big_endian);

          if (cmd_little_endian==HONEYPOT_ADD_EVIL){
              queue_put(&evil_queue, &me, 1, data_little_endian);
              // insert queue entry to evil list
          }
          else if (cmd_little_endian==HONEYPOT_ADD_VULNERABLE){

              queue_put(&vulnerable_queue, &mv, 1, data_little_endian);
              // insert queue entry to vulnerable list
          }
          else if (cmd_little_endian==HONEYPOT_ADD_SPAMMER){

              queue_put(&spammer_queue, &ms, 1, data_little_endian);
              // insert queue entry to spammer list
          }
          else if (cmd_little_endian==HONEYPOT_DEL_SPAMMER){
              queue_put(&spammer_queue, &ms, 2, data_little_endian);
              // insert queue entry to spammer list
          }
          else if (cmd_little_endian==HONEYPOT_DEL_EVIL){
              queue_put(&evil_queue, &me, 2, data_little_endian);
              // insert queue entry to evil list
          }
          else if (cmd_little_endian==HONEYPOT_DEL_VULNERABLE){
              queue_put(&vulnerable_queue, &mv, 2, data_little_endian);
              // insert queue entry to vulnerable list
          }
          else if (cmd_little_endian==HONEYPOT_PRINT){
              //print_stats();
              //hashtable_stats_table();
              queue_put(&evil_queue, &me, 3, 0);
          }
       }

       queue_put(&spammer_queue, &ms, 0, srcaddr);
       queue_put(&vulnerable_queue, &mv, 0, destport);

       unsigned int hashval=(unsigned int)hash(ptr, pkt.dma_len);
       queue_put(&evil_queue, &me, 0, hashval);

       if (mpage==0){
          //set1(current_cpu_cycles());
          mutex_lock(&mpage);
          unsigned int* temp=(unsigned int*)(physical_to_virtual(myPages.base));
          temp[myPages.last % myPages.capacity]=pkt.dma_base;
          myPages.last++;
          mutex_unlock(&mpage);
          //set2(current_cpu_cycles());
          //puts("successfully put back page");
       }
       else {
        // set1(current_cpu_cycles());
        //  printf("pkt after %u \n", pkt.dma_base);
          free_pages(physical_to_virtual(pkt.dma_base), 1);
         //         set2(current_cpu_cycles());
       }
      //printf("Going to free %x\n", pkt.dma_base);
    }
  }

  printf("Core %d of %d is alive!\n", current_cpu_id(), current_cpu_exists());

  busy_wait(current_cpu_id() * 0.1); // wait a while so messages from different cores don't get so mixed up
  int size = 64 * 1024 * 4;
  printf("about to do calloc(%d, 1)\n", size);
  unsigned int t0  = current_cpu_cycles();
  calloc(size, 1);
  unsigned int t1  = current_cpu_cycles();
  printf("DONE (%u cycles)!\n", t1 - t0);

  while (1) ;

  for (int i = 1; i < 30; i++) {
    int size = 1 << i;
    printf("about to do calloc(%d, 1)\n", size);
    calloc(size, 1);
  }



  while (1) {
    printf("Core %d is still running...\n", current_cpu_id());
    busy_wait(4.0); // wait 4 seconds
  }

  shutdown();
}

