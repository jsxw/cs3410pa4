
#include "kernel.h"
#include "ringbuffer.h"
#include "mutex.h"

volatile struct buffer buffer_pool;
volatile int mb=0;

void ringbuffer_init(){
	    struct dma_ring_slot* ring = (struct dma_ring_slot*) malloc(sizeof(struct dma_ring_slot) * BUFFER_POOL_RING_SIZE);

      buffer_pool.rx_base=(unsigned int)virtual_to_physical(ring); 
      buffer_pool.rx_capacity=BUFFER_POOL_RING_SIZE;
      buffer_pool.rx_head=0;
      buffer_pool.rx_tail=0;
  }

  /*  ptr should be physical address already
  */
  void ringbuffer_put(unsigned int ptr, unsigned int len){
        while (buffer_pool.rx_head + buffer_pool.rx_capacity <= buffer_pool.rx_tail){
        }
        int index=buffer_pool.rx_tail % buffer_pool.rx_capacity;
        struct dma_ring_slot* slot=(struct dma_ring_slot*)(physical_to_virtual(buffer_pool.rx_base)+index*sizeof(struct dma_ring_slot));
        slot->dma_base=ptr;
        slot->dma_len=len;
        buffer_pool.rx_tail++;
  }

  void ringbuffer_get(struct dma_ring_slot* slot){
  
    mutex_lock(&mb);
 
    while (buffer_pool.rx_head==buffer_pool.rx_tail){
    }

    int index=buffer_pool.rx_head % buffer_pool.rx_capacity;
    struct dma_ring_slot* temp=(struct dma_ring_slot*)(physical_to_virtual(buffer_pool.rx_base)+index*sizeof(struct dma_ring_slot));
 
    slot->dma_base=temp->dma_base;
    slot->dma_len=temp->dma_len;
    buffer_pool.rx_head++;

    mutex_unlock(&mb);
  }

  void ringbuffer_print(){
  	printf_im("head: %u\n tail: %u\n capacity: %u\n ", buffer_pool.rx_head, buffer_pool.rx_tail, buffer_pool.rx_capacity);
  }




