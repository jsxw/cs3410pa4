#ifndef HASHTABLE_H_
#define HASHTABLE_H_

/**
* Represent an arraylist
*/
typedef struct{
   void** buffer;
   unsigned int buffer_size;
   unsigned int length;
} arraylist;

struct hashtable{
   float load_factor;
   unsigned int num_of_elements;
   unsigned int table_size;
   unsigned int total_counts;
   arraylist** buckets; 
};

struct hashtable hashtable_spammer;
struct hashtable hashtable_vulnerable;
struct hashtable hashtable_evil;



void hashtable_stats(struct hashtable *self);

void hashtable_create();

void hashtable_free(struct hashtable *self);

void hashtable_put(struct hashtable *self, unsigned int key, unsigned int value, int ind);

int hashtable_get(struct hashtable *self, unsigned int key);

void hashtable_remove(struct hashtable *self, unsigned int key, int ind);

void hashtable_stats_table();

void hashtable_update(struct hashtable *self, unsigned int key, int ind);

#endif
